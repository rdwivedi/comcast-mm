<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:char="http://amdocs.com/oss/aff/schema/characteristicsContainer" xmlns:proj1="http://amdocs.com/oss/aff/schema/project" xmlns:proj="http://amdocs.com/oss/aff/schema/projectStoreElements" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
<soapenv:Header/>
<soapenv:Body>
<proj:updateProject>
<proj:updateProjectRequest>
<proj:project>
<proj1:ID>Project446</proj1:ID>
<proj1:characteristics>
<char:Characteristics>
<char:name>fabaCreatedForCust</char:name>
<char:value>true</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>allProdAssignedToBAR</char:name>
<char:value>true</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>immediateInvoiceCheck</char:name>
<char:value>true</char:value>
</char:Characteristics>
</proj1:characteristics>
</proj:project>
</proj:updateProjectRequest>
</proj:updateProject>
</soapenv:Body>
</soapenv:Envelope>
