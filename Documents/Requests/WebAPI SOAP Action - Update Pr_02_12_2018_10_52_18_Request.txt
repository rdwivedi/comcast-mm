<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:char="http://amdocs.com/oss/aff/schema/characteristicsContainer" xmlns:proj1="http://amdocs.com/oss/aff/schema/project" xmlns:proj="http://amdocs.com/oss/aff/schema/projectStoreElements" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
<soapenv:Header/>
<soapenv:Body>
<proj:updateProject>
<proj:updateProjectRequest>
<proj:project>
<proj1:ID>Project19083</proj1:ID>
<proj1:characteristics>
<char:Characteristics>
<char:name>opticalLinkBudgetHeadend</char:name>
<char:value>100.00</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>fc_Fiber_EoHFC_Design_Required</char:name>
<char:value>Yes</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>fc_Estimated_Completion_Date_Change_Reason</char:name>
<char:value>Fiber capacity issue</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>fc_Estimated_Completion_Date</char:name>
<char:value>12/3/2018</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>fc_Completion_Date</char:name>
<char:value>12/3/2018</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>fc_ETA_Change_Notes</char:name>
<char:value>ETA Notes</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>headendAddress</char:name>
<char:value>Test</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>Active_Transport</char:name>
<char:value>Yes</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>survey_Active_Transport_Cost</char:name>
<char:value>100.00</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>WavelengthTable[1].fc_f_Type_of_WDM</char:name>
<char:value>Grey</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>WavelengthTable[1].fc_f_CPE_Wavelength_TX</char:name>
<char:value>850 nm</char:value>
</char:Characteristics>
<char:Characteristics>
<char:name>WavelengthTable[1].fc_f_Hub_Wavelength_TX</char:name>
<char:value>850 nm</char:value>
</char:Characteristics>
</proj1:characteristics>
</proj:project>
</proj:updateProjectRequest>
</proj:updateProject>
</soapenv:Body>
</soapenv:Envelope>
