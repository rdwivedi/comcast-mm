<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ns3:getActivityResponse xmlns="http://amdocs.com/oss/aff/schema/executionPlan" xmlns:ns2="http://amdocs.com/oss/aff/schema/createPlan" xmlns:ns3="http://amdocs.com/oss/aff/schema/executionPlanView" xmlns:ns4="http://amdocs.com/oss/aff/schema/scheduleActivity" xmlns:ns5="http://amdocs.com/oss/aff/schema/updateMilestoneStatus" xmlns:ns6="http://amdocs.com/oss/schema/faults" xmlns:ns7="http://amdocs.com/oss/aff/schema/error" xmlns:ns8="http://amdocs.com/oss/aff/schema/unlinkPlan" xmlns:ns9="http://amdocs.com/oss/aff/schema/cancelPlan" xmlns:ns10="http://amdocs.com/oss/aff/schema/updateactivitystatus" xmlns:ns11="http://amdocs.com/oss/aff/schema/linkPlan" xmlns:ns12="http://amdocs.com/oss/aff/schema/createSubPlan" xmlns:ns13="http://amdocs.com/oss/aff/schema/linkExternalURI" xmlns:ns14="http://amdocs.com/oss/aff/schema/executePlan">
      <ns3:activity>
        <ID>70ECFB3A0E7F47899778C1D8B0C7DAA2</ID>
        <activitySpec>
          <specID>1bbe1c34-c80d-4768-88f0-0b9f7389c74d</specID>
          <versionNumber>2</versionNumber>
          <name>Activate Service on CPE (CM/NID or Fiber)</name>
          <displayName>Load CPE Config Fiber / Load Bootfile CM/NID</displayName>
          <description>Load CPE Config Fiber / Load Bootfile CM/NID</description>
          <preExecutionRule>$ProjectOrderInstance:Attributes(Name='Access_Provider'):Value = 'COMCAST' And $ProjectOrderInstance:Characteristics(Name='enableSURPort'):Value = 'pass'</preExecutionRule>
          <activityImplementation>
            <ID>c83eac55-06ef-457d-a6ce-a717bf67e4f7</ID>
            <name>ManualTaskImplementation</name>
            <description>ManualTaskActivity</description>
          </activityImplementation>
        </activitySpec>
        <operation>DO</operation>
        <executionParams>
          <executionParam>
            <name>WorkQueue</name>
            <value>EAC</value>
            <sourceType>Static</sourceType>
          </executionParam>
          <executionParam>
            <name>ProjDtls.FaCd</name>
            <value>TestData</value>
            <sourceType>Static</sourceType>
          </executionParam>
          <executionParam>
            <name>Task_Spec</name>
            <value>fb6949b3-7d9f-4822-afca-70ce064f6d29</value>
            <sourceType>Task</sourceType>
          </executionParam>
          <executionParam>
            <name>SolutionLegSiteStatus</name>
            <value>110</value>
            <sourceType>Static</sourceType>
          </executionParam>
        </executionParams>
        <projectItemInstanceID>6BF37CDF6D5E407DB7E37CB133429847</projectItemInstanceID>
        <forecastState>PENDING</forecastState>
        <businessCalendarInd>false</businessCalendarInd>
        <duration>
          <days>1</days>
          <hours>0</hours>
          <minutes>0</minutes>
        </duration>
        <needAttention>false</needAttention>
        <ns3:status>In Progress</ns3:status>
        <ns3:state>In Progress</ns3:state>
        <ns3:createDate>2018-12-27T09:04:03.162+02:00</ns3:createDate>
        <ns3:currentStartDate>2018-12-27T09:39:11.531+02:00</ns3:currentStartDate>
        <ns3:currentEndDate>2018-12-28T09:39:11.531+02:00</ns3:currentEndDate>
        <ns3:actualStartDate>2018-12-27T09:39:12.825+02:00</ns3:actualStartDate>
        <ns3:estimatedStartDate>2019-02-23T23:00:03.020+02:00</ns3:estimatedStartDate>
        <ns3:estimatedEndDate>2019-02-24T23:00:03.020+02:00</ns3:estimatedEndDate>
      </ns3:activity>
    </ns3:getActivityResponse>
  </soap:Body>
</soap:Envelope>