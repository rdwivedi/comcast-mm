<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ns3:getActivityResponse xmlns="http://amdocs.com/oss/aff/schema/executionPlan" xmlns:ns2="http://amdocs.com/oss/aff/schema/createPlan" xmlns:ns3="http://amdocs.com/oss/aff/schema/executionPlanView" xmlns:ns4="http://amdocs.com/oss/aff/schema/scheduleActivity" xmlns:ns5="http://amdocs.com/oss/aff/schema/updateMilestoneStatus" xmlns:ns6="http://amdocs.com/oss/schema/faults" xmlns:ns7="http://amdocs.com/oss/aff/schema/error" xmlns:ns8="http://amdocs.com/oss/aff/schema/unlinkPlan" xmlns:ns9="http://amdocs.com/oss/aff/schema/cancelPlan" xmlns:ns10="http://amdocs.com/oss/aff/schema/updateactivitystatus" xmlns:ns11="http://amdocs.com/oss/aff/schema/linkPlan" xmlns:ns12="http://amdocs.com/oss/aff/schema/createSubPlan" xmlns:ns13="http://amdocs.com/oss/aff/schema/linkExternalURI" xmlns:ns14="http://amdocs.com/oss/aff/schema/executePlan">
      <ns3:activity>
        <ID>3B5140482C71477F8BC80E42028BC52F</ID>
        <activitySpec>
          <specID>2636b956-9fdd-482f-a37b-79dc19f59ae7</specID>
          <versionNumber>2</versionNumber>
          <name>Construction Payment And Security Deposit</name>
          <displayName>Construction Payment And Security Deposit</displayName>
          <description>Construction Payment And Security Deposit</description>
          <activityImplementation>
            <ID>c83eac55-06ef-457d-a6ce-a717bf67e4f7</ID>
            <name>ManualTaskImplementation</name>
            <description>ManualTaskActivity</description>
          </activityImplementation>
        </activitySpec>
        <operation>DO</operation>
        <executionParams>
          <executionParam>
            <name>WorkQueue</name>
            <value>OPE</value>
            <sourceType>Static</sourceType>
          </executionParam>
          <executionParam>
            <name>Task_Spec</name>
            <value>9987b713-c3f0-4ce5-824a-b8753c9bc4ad</value>
            <sourceType>Task</sourceType>
          </executionParam>
          <executionParam>
            <name>ProjDtls.FaCd</name>
            <value>TestData</value>
            <sourceType>Static</sourceType>
          </executionParam>
        </executionParams>
        <projectItemInstanceID>5F0C66E3312944C39B1ED38F2932E380</projectItemInstanceID>
        <forecastState>CONFIRMED</forecastState>
        <businessCalendarInd>false</businessCalendarInd>
        <duration>
          <days>1</days>
          <hours>0</hours>
          <minutes>0</minutes>
        </duration>
        <needAttention>false</needAttention>
        <ns3:status>In Progress</ns3:status>
        <ns3:state>In Progress</ns3:state>
        <ns3:createDate>2018-12-02T15:11:53.050+02:00</ns3:createDate>
        <ns3:currentStartDate>2018-12-02T15:12:55.721+02:00</ns3:currentStartDate>
        <ns3:currentEndDate>2018-12-03T15:12:55.721+02:00</ns3:currentEndDate>
        <ns3:actualStartDate>2018-12-02T15:12:56.024+02:00</ns3:actualStartDate>
        <ns3:estimatedStartDate>2018-12-07T15:12:27.052+02:00</ns3:estimatedStartDate>
        <ns3:estimatedEndDate>2018-12-08T15:12:27.052+02:00</ns3:estimatedEndDate>
      </ns3:activity>
    </ns3:getActivityResponse>
  </soap:Body>
</soap:Envelope>