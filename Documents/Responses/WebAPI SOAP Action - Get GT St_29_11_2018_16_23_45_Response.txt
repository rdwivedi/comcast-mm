<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ns3:getActivityResponse xmlns="http://amdocs.com/oss/aff/schema/executionPlan" xmlns:ns2="http://amdocs.com/oss/aff/schema/createPlan" xmlns:ns3="http://amdocs.com/oss/aff/schema/executionPlanView" xmlns:ns4="http://amdocs.com/oss/aff/schema/scheduleActivity" xmlns:ns5="http://amdocs.com/oss/aff/schema/updateMilestoneStatus" xmlns:ns6="http://amdocs.com/oss/schema/faults" xmlns:ns7="http://amdocs.com/oss/aff/schema/error" xmlns:ns8="http://amdocs.com/oss/aff/schema/unlinkPlan" xmlns:ns9="http://amdocs.com/oss/aff/schema/cancelPlan" xmlns:ns10="http://amdocs.com/oss/aff/schema/updateactivitystatus" xmlns:ns11="http://amdocs.com/oss/aff/schema/linkPlan" xmlns:ns12="http://amdocs.com/oss/aff/schema/createSubPlan" xmlns:ns13="http://amdocs.com/oss/aff/schema/linkExternalURI" xmlns:ns14="http://amdocs.com/oss/aff/schema/executePlan">
      <ns3:activity>
        <ID>AA7FE3E2F61C4611A0DE92274993DE95</ID>
        <activitySpec>
          <specID>da2e4e02-9361-4ba3-baf9-47c1439c2548</specID>
          <versionNumber>2</versionNumber>
          <name>DNS Registration Equipment Assign</name>
          <displayName>DNS Registration</displayName>
          <preExecutionRule>$ProjectOrderInstance:Attributes(Name='Access_Provider'):Value = 'Comcast' And $ProjectOrderItem:Characteristics(Name='Transport_Type'):Value = 'Fiber'</preExecutionRule>
          <activityImplementation>
            <ID>90c55386-7841-4053-a278-4d57825b0d55</ID>
            <name>DNS Implementation</name>
          </activityImplementation>
        </activitySpec>
        <operation>DO</operation>
        <executionParams />
        <projectItemInstanceID>EAA57C1080BA4F618BABA1CA53814CA2</projectItemInstanceID>
        <forecastState>PENDING</forecastState>
        <businessCalendarInd>false</businessCalendarInd>
        <duration>
          <days>0</days>
          <hours>0</hours>
          <minutes>1</minutes>
        </duration>
        <needAttention>false</needAttention>
        <errorDetail>
          <errorID>6C57422DA8BA4A009E63FCB64E5E2A80</errorID>
          <errorCode>DNS_ERROR</errorCode>
        </errorDetail>
        <ns3:status>Not Started</ns3:status>
        <ns3:state>In Error</ns3:state>
        <ns3:createDate>2018-11-29T16:01:38.720+02:00</ns3:createDate>
        <ns3:currentStartDate>2018-11-29T16:22:48.300+02:00</ns3:currentStartDate>
        <ns3:currentEndDate>2018-11-29T16:23:48.300+02:00</ns3:currentEndDate>
        <ns3:actualStartDate>2018-11-29T16:22:48.773+02:00</ns3:actualStartDate>
        <ns3:estimatedStartDate>2018-12-17T16:07:37.309+02:00</ns3:estimatedStartDate>
        <ns3:estimatedEndDate>2018-12-17T16:08:37.309+02:00</ns3:estimatedEndDate>
      </ns3:activity>
    </ns3:getActivityResponse>
  </soap:Body>
</soap:Envelope>