<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ns3:getActivityResponse xmlns="http://amdocs.com/oss/aff/schema/executionPlan" xmlns:ns2="http://amdocs.com/oss/aff/schema/createPlan" xmlns:ns3="http://amdocs.com/oss/aff/schema/executionPlanView" xmlns:ns4="http://amdocs.com/oss/aff/schema/scheduleActivity" xmlns:ns5="http://amdocs.com/oss/aff/schema/updateMilestoneStatus" xmlns:ns6="http://amdocs.com/oss/schema/faults" xmlns:ns7="http://amdocs.com/oss/aff/schema/error" xmlns:ns8="http://amdocs.com/oss/aff/schema/unlinkPlan" xmlns:ns9="http://amdocs.com/oss/aff/schema/cancelPlan" xmlns:ns10="http://amdocs.com/oss/aff/schema/updateactivitystatus" xmlns:ns11="http://amdocs.com/oss/aff/schema/linkPlan" xmlns:ns12="http://amdocs.com/oss/aff/schema/createSubPlan" xmlns:ns13="http://amdocs.com/oss/aff/schema/linkExternalURI" xmlns:ns14="http://amdocs.com/oss/aff/schema/executePlan">
      <ns3:activity>
        <ID>8CEE58C7C8BB472982D1F3BA7A9B61CD</ID>
        <activitySpec>
          <specID>9f879f39-f401-4b7f-a0d1-5df8ef9a67ec</specID>
          <versionNumber>2</versionNumber>
          <name>Radius Registration Equipment Assign</name>
          <displayName>RADIUS Registration</displayName>
          <preExecutionRule>$ProjectOrderInstance:Attributes(Name='Access_Provider'):Value = 'Comcast' And $ProjectOrderItem:Characteristics(Name='Transport_Type'):Value = 'Fiber'</preExecutionRule>
          <activityImplementation>
            <ID>2353e2a0-d3f4-40bf-a5b4-71777f8b130d</ID>
            <name>Radious Implementation</name>
          </activityImplementation>
        </activitySpec>
        <operation>DO</operation>
        <executionParams />
        <projectItemInstanceID>EAA57C1080BA4F618BABA1CA53814CA2</projectItemInstanceID>
        <forecastState>PENDING</forecastState>
        <businessCalendarInd>false</businessCalendarInd>
        <duration>
          <days>0</days>
          <hours>0</hours>
          <minutes>1</minutes>
        </duration>
        <needAttention>false</needAttention>
        <errorDetail>
          <errorID>7863E31978654E0784072650D8B30839</errorID>
          <errorCode>RADIUS_ERROR</errorCode>
        </errorDetail>
        <ns3:status>Not Started</ns3:status>
        <ns3:state>In Error</ns3:state>
        <ns3:createDate>2018-11-29T16:01:38.720+02:00</ns3:createDate>
        <ns3:currentStartDate>2018-11-29T16:19:47.056+02:00</ns3:currentStartDate>
        <ns3:currentEndDate>2018-11-29T16:20:47.056+02:00</ns3:currentEndDate>
        <ns3:actualStartDate>2018-11-29T16:19:49.815+02:00</ns3:actualStartDate>
        <ns3:estimatedStartDate>2018-12-17T16:06:37.309+02:00</ns3:estimatedStartDate>
        <ns3:estimatedEndDate>2018-12-17T16:07:37.309+02:00</ns3:estimatedEndDate>
      </ns3:activity>
    </ns3:getActivityResponse>
  </soap:Body>
</soap:Envelope>