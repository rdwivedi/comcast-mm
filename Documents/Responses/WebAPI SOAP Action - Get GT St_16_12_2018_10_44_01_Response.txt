<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ns3:getActivityResponse xmlns="http://amdocs.com/oss/aff/schema/executionPlan" xmlns:ns2="http://amdocs.com/oss/aff/schema/createPlan" xmlns:ns3="http://amdocs.com/oss/aff/schema/executionPlanView" xmlns:ns4="http://amdocs.com/oss/aff/schema/scheduleActivity" xmlns:ns5="http://amdocs.com/oss/aff/schema/updateMilestoneStatus" xmlns:ns6="http://amdocs.com/oss/schema/faults" xmlns:ns7="http://amdocs.com/oss/aff/schema/error" xmlns:ns8="http://amdocs.com/oss/aff/schema/unlinkPlan" xmlns:ns9="http://amdocs.com/oss/aff/schema/cancelPlan" xmlns:ns10="http://amdocs.com/oss/aff/schema/updateactivitystatus" xmlns:ns11="http://amdocs.com/oss/aff/schema/linkPlan" xmlns:ns12="http://amdocs.com/oss/aff/schema/createSubPlan" xmlns:ns13="http://amdocs.com/oss/aff/schema/linkExternalURI" xmlns:ns14="http://amdocs.com/oss/aff/schema/executePlan">
      <ns3:activity>
        <ID>81B26A68E5D34BA8BCB0499A1A46D2C3</ID>
        <activitySpec>
          <specID>9f879f39-f401-4b7f-a0d1-5df8ef9a67ec</specID>
          <versionNumber>2</versionNumber>
          <name>Radius Registration Equipment Assign</name>
          <displayName>RADIUS Registration</displayName>
          <preExecutionRule>$ProjectOrderInstance:Attributes(Name='Access_Provider'):Value = 'Comcast' And $ProjectOrderItem:Characteristics(Name='Transport_Type'):Value = 'Fiber'</preExecutionRule>
          <activityImplementation>
            <ID>2353e2a0-d3f4-40bf-a5b4-71777f8b130d</ID>
            <name>Radious Implementation</name>
          </activityImplementation>
        </activitySpec>
        <operation>DO</operation>
        <executionParams />
        <projectItemInstanceID>7F7E2A69586F485D97AC5160C2D6FFAD</projectItemInstanceID>
        <forecastState>PENDING</forecastState>
        <businessCalendarInd>false</businessCalendarInd>
        <duration>
          <days>0</days>
          <hours>0</hours>
          <minutes>1</minutes>
        </duration>
        <needAttention>false</needAttention>
        <errorDetail>
          <errorID>EA41C2B191CF47F6A20FF13C3BB17B2B</errorID>
          <errorCode>RADIUS_ERROR</errorCode>
        </errorDetail>
        <ns3:status>Not Started</ns3:status>
        <ns3:state>In Error</ns3:state>
        <ns3:createDate>2018-12-16T10:30:43.391+02:00</ns3:createDate>
        <ns3:currentStartDate>2018-12-17T10:41:35.976+02:00</ns3:currentStartDate>
        <ns3:currentEndDate>2018-12-17T10:42:35.976+02:00</ns3:currentEndDate>
        <ns3:actualStartDate>2018-12-16T10:43:44.048+02:00</ns3:actualStartDate>
        <ns3:estimatedStartDate>2019-01-03T10:35:42.763+02:00</ns3:estimatedStartDate>
        <ns3:estimatedEndDate>2019-01-03T10:36:42.763+02:00</ns3:estimatedEndDate>
      </ns3:activity>
    </ns3:getActivityResponse>
  </soap:Body>
</soap:Envelope>