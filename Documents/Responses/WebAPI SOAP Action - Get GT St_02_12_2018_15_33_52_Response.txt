<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ns3:getActivityResponse xmlns="http://amdocs.com/oss/aff/schema/executionPlan" xmlns:ns2="http://amdocs.com/oss/aff/schema/createPlan" xmlns:ns3="http://amdocs.com/oss/aff/schema/executionPlanView" xmlns:ns4="http://amdocs.com/oss/aff/schema/scheduleActivity" xmlns:ns5="http://amdocs.com/oss/aff/schema/updateMilestoneStatus" xmlns:ns6="http://amdocs.com/oss/schema/faults" xmlns:ns7="http://amdocs.com/oss/aff/schema/error" xmlns:ns8="http://amdocs.com/oss/aff/schema/unlinkPlan" xmlns:ns9="http://amdocs.com/oss/aff/schema/cancelPlan" xmlns:ns10="http://amdocs.com/oss/aff/schema/updateactivitystatus" xmlns:ns11="http://amdocs.com/oss/aff/schema/linkPlan" xmlns:ns12="http://amdocs.com/oss/aff/schema/createSubPlan" xmlns:ns13="http://amdocs.com/oss/aff/schema/linkExternalURI" xmlns:ns14="http://amdocs.com/oss/aff/schema/executePlan">
      <ns3:activity>
        <ID>8C152B48DABD428F954F54612D474619</ID>
        <activitySpec>
          <specID>19dee07b-742b-4b36-ba35-192b6b7cbbc4</specID>
          <versionNumber>2</versionNumber>
          <name>Test Service Snapshot</name>
          <displayName>Test Service Snapshot</displayName>
          <description>Manual Task to Troubleshoot Test Service Issues</description>
          <preExecutionRule>$ProjectOrderInstance:Attributes(Name='Access_Provider'):Value = 'Comcast' And  ( $ProjectOrderItem:Characteristics(Name='HOTCUT'):Value = 'No' Or  ( $ProjectOrderItem:Characteristics(Name='HOTCUT'):Value = 'Yes' And $ProjectOrderItem:Characteristics(Name='testServiceRequired'):Value = 'Yes' )  )  And  ( $ProjectOrderItem:Characteristics(Name='Test_Type'):Value != 'MANUAL' Or $ProjectOrderItem:Characteristics(Name='testServiceStatus'):Value != 'pass' )  And  ( $ProjectOrderInstance:Attributes(Name='TTU_OtherEndAccessType'):Value = 'Off-net' Or  ( $ProjectOrderInstance:Attributes(Name='TTU_SyncFlag'):Value = 'true' And $ProjectOrderInstance:Attributes(Name='TTU_OtherEndAccessType'):Value = 'On-net' )  )  And $ProjectOrderInstance:Attributes(Name='TTUTestService.Arrival'):Value = 'Second'</preExecutionRule>
          <activityImplementation>
            <ID>c83eac55-06ef-457d-a6ce-a717bf67e4f7</ID>
            <name>ManualTaskImplementation</name>
            <description>ManualTaskActivity</description>
          </activityImplementation>
        </activitySpec>
        <operation>DO</operation>
        <executionParams>
          <executionParam>
            <name>WorkQueue</name>
            <value>EAC</value>
            <sourceType>Static</sourceType>
          </executionParam>
          <executionParam>
            <name>ProjDtls.FaCd</name>
            <value>TestData</value>
            <sourceType>Static</sourceType>
          </executionParam>
          <executionParam>
            <name>Task_Spec</name>
            <value>7daaf10f-b7c0-4dcc-9c01-5b71e7790ff9</value>
            <sourceType>Task</sourceType>
          </executionParam>
          <executionParam>
            <name>SolutionLegSiteStatus</name>
            <value>121</value>
            <sourceType>Static</sourceType>
          </executionParam>
        </executionParams>
        <projectItemInstanceID>8A28416D6A76411AB29740BC601186A7</projectItemInstanceID>
        <forecastState>PENDING</forecastState>
        <businessCalendarInd>false</businessCalendarInd>
        <duration>
          <days>1</days>
          <hours>0</hours>
          <minutes>0</minutes>
        </duration>
        <needAttention>false</needAttention>
        <ns3:status>Not Started</ns3:status>
        <ns3:state>Skipped</ns3:state>
        <ns3:createDate>2018-12-02T15:14:48.078+02:00</ns3:createDate>
        <ns3:completeDate>2018-12-02T15:33:45.581+02:00</ns3:completeDate>
        <ns3:currentStartDate>2018-12-02T15:33:45.632+02:00</ns3:currentStartDate>
        <ns3:currentEndDate>2018-12-02T15:33:45.632+02:00</ns3:currentEndDate>
        <ns3:estimatedStartDate>2019-02-12T23:00:44.361+02:00</ns3:estimatedStartDate>
        <ns3:estimatedEndDate>2019-02-13T23:00:44.361+02:00</ns3:estimatedEndDate>
      </ns3:activity>
    </ns3:getActivityResponse>
  </soap:Body>
</soap:Envelope>