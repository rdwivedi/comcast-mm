<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <soap:Fault>
      <faultcode>soap:Server</faultcode>
      <faultstring>Plan not found for entity: ERROR: The Variable 'BF_PLAN_ID' was not found</faultstring>
      <detail>
        <ns6:exception xmlns:ns9="http://amdocs.com/oss/aff/schema/cancelPlan" xmlns:ns8="http://amdocs.com/oss/aff/schema/unlinkPlan" xmlns:ns7="http://amdocs.com/oss/aff/schema/error" xmlns:ns6="http://amdocs.com/oss/schema/faults" xmlns:ns5="http://amdocs.com/oss/aff/schema/updateMilestoneStatus" xmlns:ns4="http://amdocs.com/oss/aff/schema/scheduleActivity" xmlns:ns3="http://amdocs.com/oss/aff/schema/executionPlanView" xmlns:ns2="http://amdocs.com/oss/aff/schema/createPlan" xmlns:ns14="http://amdocs.com/oss/aff/schema/executePlan" xmlns:ns13="http://amdocs.com/oss/aff/schema/linkExternalURI" xmlns:ns12="http://amdocs.com/oss/aff/schema/createSubPlan" xmlns:ns11="http://amdocs.com/oss/aff/schema/linkPlan" xmlns:ns10="http://amdocs.com/oss/aff/schema/updateactivitystatus" xmlns="http://amdocs.com/oss/aff/schema/executionPlan">
          <ns6:code>Client.PlanNotFound</ns6:code>
          <ns6:reason>Plan not found for entity: ERROR: The Variable 'BF_PLAN_ID' was not found</ns6:reason>
        </ns6:exception>
      </detail>
    </soap:Fault>
  </soap:Body>
</soap:Envelope>