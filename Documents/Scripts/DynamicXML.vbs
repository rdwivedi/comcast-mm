Dim fso, oExcel, wb, ws, ProjectID,Active_GT

Set fso = WScript.CreateObject("Scripting.Filesystemobject")
Set oExcel = WScript.CreateObject("Excel.Application")
'oExcel.Visible = True

Work_path=fso.GetAbsolutePathName("C:\workspace\Comcast_E2E_Automation\COMCAST\Documents\XMLs")

Set XmlFile=fso.CreateTextFile(Work_Path&"\ProjectStore.xml",True,True)
Set wb=oExcel.Workbooks.Open("C:\workspace\Comcast_E2E_Automation\COMCAST\Documents\Data.xlsx")
Set ws=wb.Sheets(3)

XmlFile.Write("<?xml version=""1.0"" encoding=""UTF-8""?>"&vbNewLine)
XmlFile.Write("<soapenv:Envelope xmlns:char=""http://amdocs.com/oss/aff/schema/characteristicsContainer"" xmlns:proj1=""http://amdocs.com/oss/aff/schema/project"" xmlns:proj=""http://amdocs.com/oss/aff/schema/projectStoreElements"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">"&vbNewLine)
XmlFile.Write("<soapenv:Header/>"&vbNewLine)
XmlFile.Write("<soapenv:Body>"&vbNewLine)
XmlFile.Write("<proj:updateProject>"&vbNewLine)
XmlFile.Write("<proj:updateProjectRequest>"&vbNewLine)
XmlFile.Write("<proj:project>"&vbNewLine)
XmlFile.Write("<proj1:ID>{ODO_PROJECT_ID}</proj1:ID>"&vbNewLine)
Row_count=ws.UsedRange.Rows.Count
Column_count=ws.UsedRange.Columns.Count

For i=1 To Row_count
	if ws.cells(i,1)= "NO" Then
		Active_GT=i
		'MsgBox Active_GT
		Exit For
	End If
Next

XmlFile.Write("<proj1:characteristics>"&vbNewLine)

For j=0 To Column_count Step 2

	if ws.cells(Active_GT,j+3)<>  "" Then
	
		CName=ws.cells(Active_GT,j+3) 
		CValue=ws.cells(Active_GT,j+4)
		
		
		XmlFile.Write("<char:Characteristics>"&vbNewLine)
		XmlFile.Write("<char:name>"&CName&"</char:name>"&vbNewLine)
		XmlFile.Write("<char:value>"&CValue&"</char:value>"&vbNewLine)
		XmlFile.Write("</char:Characteristics>"&vbNewLine)
	End if
Next


XmlFile.Write("</proj1:characteristics>"&vbNewLine)
XmlFile.Write("</proj:project>"&vbNewLine)
XmlFile.Write("</proj:updateProjectRequest>"&vbNewLine)
XmlFile.Write("</proj:updateProject>"&vbNewLine)
XmlFile.Write("</soapenv:Body>"&vbNewLine)
XmlFile.Write("</soapenv:Envelope>"&vbNewLine)
'ws.cells(Active_GT,1).Value="YES"
wb.Save
wb.Close
oExcel.Quit









