import java.util.ArrayList
import java.util.HashMap
import java.util.LinkedHashMap
import java.util.Set
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import com.comcast.logging.logtransactions.*;
import java.text.SimpleDateFormat;
import java.io.File;
import java.util.List;
import groovy.time.*



def timeStart = new Date()
println "timeStart->"+timeStart

String businessFlowName="";
String busEnv="";
String busStTm="";
String busEdTm="";
String busDur="";
String busStatus="";

long bStMs;
long bEdMs;

def elasticSearchURL = "http://10.124.4.252:9201/orionmm/transaction"

String reportParentDir = "C:\\GingerMM_Jenkins_Reports\\"

String latestDir = getLastModifiedDirectory(reportParentDir)

File dir= new File(latestDir);

def url= searchDirectory(dir, "BusinessFlowReport.html");

println "FOUND URL --------------------------->"+url

File input = new File(url);

Document doc = Jsoup.parse(input, "UTF-8", "");

def tables = doc.select("table")
println "Number of Tables in HTML report ->" + tables.size();

LinkedHashMap<String, String> busFlowLHM = new LinkedHashMap<String, String>();

ArrayList actList = new ArrayList();

int index = 0;
for(Element table : tables) {
	println "Table : " + (++index) + " has " + table.select("tr").size() + " rows.";

	if(index == 2) {
		int x=0;
		def tbRows = table.select("tr")
		// For each column in a row, print its contents
		tbRows.each { row ->
			x++;
			if(x==1)
				return;
			String brData="";
			def tbCol = row.select("td")
			tbCol.each { column ->
				brData = brData + column.text()+"^";
			}
			if(! brData.equals(""))
			{
				println "Business Flow Table Row "+x+ "->" + brData
				brData2 = brData;
				int ct = brData2.length() - brData2.replace("^", "").length();
				SimpleDateFormat f1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss aaa");
				if(ct == 12)
				{
					String[] sp2 = brData2.split ("\\^");
					for (int i = 0; i<sp2.length; i++)
					{
						if(i==1)
						{
							businessFlowName= sp2[i];
						}
						if(i==5)
						{
							busEnv= sp2[i];
						}
						if(i==6)
						{
							busStTm= sp2[i];
							try
							{
								Date stDt1 = f1.parse(busStTm);
								bStMs = stDt1.getTime();
								println "BF converted start date->"+bStMs
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						if(i==7)
						{
							busEdTm= sp2[i];
							try
							{
								Date edDt1 = f1.parse(busEdTm);
								bEdMs = edDt1.getTime();
								println "BF converted end date->"+bEdMs
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						if(i==8)
						{
							busDur= sp2[i];
						}
						if(i==9)
						{
							busStatus= sp2[i];
						}

						busFlowLHM.put(sp2[1],sp2);
					}
				}
			}
		}
	}


	LinkedHashMap<String, String> parentLHM = new LinkedHashMap<String, String>();
	LinkedHashMap<String, LinkedHashMap<String, String>> childHoH = new LinkedHashMap<String, LinkedHashMap<String, String>>();
	HashMap<String, LinkedHashMap<String, String>> outputMap = new HashMap<String, LinkedHashMap<String, String>>();


	if(index == 4 )
	{
		def tbRows = table.select("tr")
		int r=0;
		String parentKey ="";
		tbRows.each { row ->

			LinkedHashMap<String, String> childLHM = new LinkedHashMap<String, String>();
			r++;
			String rowData="";

			def tbCol = row.select("td")
			tbCol.each { column ->

				if(! column.text().startsWith("Execution Sequence Description Execution Start Time"))
				{
					rowData = rowData + column.text()+"^";
				}
				else
				{
					return;
				}
			}

			boolean foundColHdg=false;
			Set<String> keys3 = parentLHM.keySet();
			for(String key:keys3){
				if (key != null && key.equals("Execution Sequence")) {
					foundColHdg=true;
					break;
				}
			}

			if(rowData.contains("Execution Status") && foundColHdg)
			{
				rowData = "";
			}

			if(! rowData.equals(""))
			{
				rowData2 = rowData;
				int count = rowData2.length() - rowData2.replace("^", "").length();
				if(count == 11)
				{
					if(!parentKey.equals(""))
					{
						outputMap.put(parentKey,childHoH);
						childHoH = new LinkedHashMap<String, LinkedHashMap<String, String>>();
					}
					String[] sp = rowData2.split ("\\^");
					parentKey =sp[0];
					//println "parentKey ="+parentKey;

					for (int i = 0; i<sp.length; i++)
						parentLHM.put(sp[0],sp);
				}
				if(count == 12)
				{
					String[] sp = rowData2.split ("\\^");
					for (int i = 0; i<sp.length; i++)
					{
						childLHM.put(sp[0],sp);
					}
					childHoH.put(sp[0],childLHM);
				}
			}
		}
	}

	Set<String> keys = parentLHM.keySet();
	for(String key:keys){
		if (key != null ) {
			println "Activity Table Row key ->" + key;
			println "Activity Table Row value ->" + parentLHM.get(key);
			if(! key.equals("Execution Sequence"))
			{
				actList.add(parentLHM.get(key));
			}
		}
	}

}

String hostAddress = "TO BE FILLED";//JENKINS ENV VAR - NODE_NAME


InetAddress localhost = null;

try
{
	localhost = InetAddress.getLocalHost();
	println( "InetAddress: " + localhost );
	hostAddress = localhost.getHostName();
	println( "\tHost Name: " + hostAddress);
	
}
catch (Exception ue) // checked exception
{
   println(  "Unknown Host ("
					  + ue.getClass().toString()
					  + "): " + ue.getMessage() );
}


String uuid = UUID.randomUUID()
String masterJob = "MASTER_" + "Orion MidMarket" + "_" + uuid

Properties transLogParams = new Properties();

transLogParams.setProperty("ESURL", elasticSearchURL);

transLogParams.setProperty("APPLICATION","Orion MidMarket");
transLogParams.setProperty("TEST_NAME", businessFlowName);
transLogParams.setProperty("APP_ENV",busEnv);
transLogParams.setProperty("MASTER_JOB", masterJob);
transLogParams.setProperty("PERF_LOCATION","C:\\PerformanceMetrics");
transLogParams.setProperty("TRANSACTION_TYPE","Test");
transLogParams.setProperty("TEST_TYPE","BVT");
transLogParams.setProperty("HOST", hostAddress);
LoggerMainImpl logImpl = new LoggerMainImpl(transLogParams);

logImpl.changeHeader("START_TIME,TRANSACTION_NAME,TRANSACTION_TYPE,DURATION_MS,STATUS,TEST_NAME,METHOD_NAME,TRANSACTIONEND_MESSAGE,END_TIME,TRANSACTION_DATA,APPLICATION,RELEASE_VERSION,BUILD_NO,HOST,APP_ENV,USERID,APP_EXCEPTION_MSG,TEST_TYPE,MASTER_JOB")


println "businessFlowName->"+businessFlowName
println "busEnv->"+busEnv
println "masterJob->"+masterJob

println "bStMs->"+bStMs
println "bEdMs->"+bEdMs
println "busDur->"+busDur
println "busStatus->"+busStatus

transLogParams.setProperty("DURATION_MS", busDur);

logImpl.logTransaction(businessFlowName, bStMs, bEdMs, busStatus, transLogParams)



long stMs;
long edMs;

println "actList.size()->"+actList.size()

for(int y=0; y<actList.size();y++)
{
	transLogParams.setProperty("TRANSACTION_TYPE","Method");
	String[] activtyArr = actList.get(y);

	String actyName="";
	String actyStTm="";
	String actyEdTm="";
	String actyDur="";
	String actyStatus="";

	println "activtyArr ->"+ activtyArr
	for (int i = 0; i<activtyArr.length; i++)
	{
		if(i==2)
		{
			actyName= activtyArr[i];
			println "actyName ->"+ actyName
			transLogParams.setProperty("METHOD_NAME", actyName);
		}
		if(i==5)
		{
			actyStTm= activtyArr[i];
			println "actyStTm ->"+ actyStTm
		}
		if(i==6)
		{
			actyEdTm= activtyArr[i];
			println "actyEdTm ->"+ actyEdTm
		}
		if(i==7)
		{
			actyDur= activtyArr[i];
			println "actyDur ->"+ actyDur
			transLogParams.setProperty("DURATION_MS", actyDur);
		}
		if(i==8)
		{
			actyStatus= activtyArr[i];
			println "actyStatus ->"+ actyStatus
		}
	}
	SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss aaa");
	try {
		Date stDt = f.parse(actyStTm);
		Date edDt = f.parse(actyEdTm);
		stMs = stDt.getTime();
		edMs = edDt.getTime();
		//println "converted start date->"+stMs
		//println "converted end date->"+edMs
	} catch (Exception e) {
		e.printStackTrace();
	}

	logImpl.logTransaction(actyName, stMs, edMs, actyStatus, transLogParams)
}

// Some code you want to time
def timeStop = new Date()
println "timeStop->"+timeStop
TimeDuration duration1 = TimeCategory.minus(timeStop, timeStart)
println "Total time to execute script->"+duration1


////////////////////////////////////////////////////////////////////////

public String search(File file, String fileNameToSearch) {

	String res="";
	if (file.isDirectory())
	{
		//do you have permission to read this directory?
		if (file.canRead()) {
			for (File temp : file.listFiles()) {
				if (temp.isDirectory()) {
					String rr = search(temp,fileNameToSearch);
					if(! rr.equals(""))
					{
						res=rr;
						break;
					}
				} else {
					if (fileNameToSearch.equalsIgnoreCase(temp.getName())) {
						res = temp.getAbsoluteFile().toString();						
						break;
					}

				}
			}
		} else {
			println ""+file.getAbsoluteFile() + "Permission Denied";
		}
	}
	return res;

}

public String searchDirectory(File directory, String fileNameToSearch) {
	String res1="";
	if (directory.isDirectory()) {
		res1 = search(directory,fileNameToSearch);
	} else {
		println ""+directory.getAbsoluteFile() + " is not a directory!";
	}
	return res1;
}

static def updateProperties(propertiesFileName) {
	Properties transParams = new Properties()
	File propertiesFile = new File(propertiesFileName)
		propertiesFile.withInputStream {
		transParams.load(it)
		}
		return (transParams)
	   
}


static def getLastModifiedFile (directory)
 {
	return new File(directory).listFiles()?.sort { -it.lastModified() }?.head().toString()
 }

 static def  getLastModifiedDirectory (directory)
 {
	 File dir = new File(directory);
	 File max = null;
	 for (File file : dir.listFiles())
	 {
		if (file.isDirectory() && (max == null || max.lastModified() < file.lastModified()))
		{
			max = file;
		}
	 }
	 println "max--------------------------->"+max
	 return max
	 
	 
 }
