'get script arguments
On Error Resume Next

Set args = WScript.Arguments
sConfigFile= args.Item(0)
sSolName= args.Item(1)
sEnvCode= args.Item(2)
sRunSet= args.Item(3)

WScript.StdOut.WriteLine
WScript.StdOut.WriteLine "### Running with the following arguments: ###"
WScript.StdOut.WriteLine "sConfigFile = " & sConfigFile
WScript.StdOut.WriteLine "sSolName = " & sSolName
WScript.StdOut.WriteLine "sEnvCode = " & sEnvCode
WScript.StdOut.WriteLine "sRunSet = " & sRunSet

Set objFSO=CreateObject("Scripting.FileSystemObject")

' How to write file

Set objFile = objFSO.CreateTextFile(sConfigFile,True)
objFile.WriteLine "Solution=" & sSolName
objFile.WriteLine "Env=" & sEnvCode
objFile.WriteLine "RunSet=" & sRunSet
objFile.Close
