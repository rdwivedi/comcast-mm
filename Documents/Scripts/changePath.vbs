Dim objWSH
Dim objUserVariables
Dim objSystemVariables
Dim path

Set objWSH =  CreateObject("WScript.Shell")
Set objUserVariables = objWSH.Environment("USER")

Const ForReading = 1
Const ForWriting = 2

path = objUserVariables("GINGER")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.OpenTextFile(path & "\Scripts\DynamicXML.vbs", ForReading)
strText = objFile.ReadAll
objFile.Close

strNewText = Replace(strText, "C:\workspace\Comcast_E2E_Automation\COMCAST\Documents", objUserVariables("GINGER"))
Set objFile = objFSO.OpenTextFile(path & "\Scripts\DynamicXML.vbs", ForWriting)

objFile.WriteLine strNewText
objFile.Close

Wscript.echo(path)