#!/bin/bash
# This is a comment!
# Make sure env contain the properties PROPERTY_FILE
#Modifede for ginger use i/o

PROPERTY_FILE=${HOME}'/pushToCC/dbp/delivery.properties'


function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`cat $PROPERTY_FILE 2>/dev/null | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}
checkConn()
{
URL=$1
NAME=$2
wget_output=$(wget -q "$URL")
if [ $? -ne 0 ]; then
echo "${NAME}=OK"
else
echo "${NAME}=NOT OK"
fi
}

checkCurl()
{
#echo $1
response=$(curl -s -o /dev/null -w "%{http_code}"  $1)
NAME=$2
if [ ${response} == 200 ] || [ ${response} == 500 ] || [ ${response} == 405 ] || [ ${response} == 403 ] ; then
echo "${NAME}=OK"
else
echo "${NAME}=NOT OK"
fi
}

checkDiskSpace()
{
printf "DISK_SPACE="
df . | awk 'END{print $5}'
}

checkCPQEndpoint()
{
#printf "==> Connectivity with CPQ endpoint: \n"
REPOSITORY_URL=$(getProperty "aff.cpq.service.ws.endpoint.baseURL")
checkConn "${REPOSITORY_URL}" "checkCPQEndpoint"
}

checkAmilUrlForCpm()
{
#printf "==> Connectivity with AMIL URL FOR CPM: \n"
AMIL_URL_FOR_CPM=$(getProperty "AMIL_URL_FOR_CPM")
checkConn "${AMIL_URL_FOR_CPM}" "checkAmilUrlForCpm"
}


checkAmilUrlForEmm()
{
#printf "==> Connectivity with AMIL_URL_FOR_EMM: \n"
AMIL_URL_FOR_EMM=$(getProperty "AMIL_URL_FOR_EMM")
checkConn "${AMIL_URL_FOR_EMM}" "checkAmilUrlForEmm"
}

checkLIC_SQO_HOST()
{
#printf "==> Connectivity with LIC.SQO.HOST: \n"
LIC_SQO_HOST=$(getProperty "LIC.SQO.HOST")
checkConn "${LIC_SQO_HOST}" "checkLic_SQO"
}

checkSQO_HOST()
{
#printf "==> Connectivity with SQO_URL: \n"
SQO_URL=$(getProperty "SQO_URL")
##printf ${SQO_URL}

checkCurl "${SQO_URL}" "checkSQO_Host"

}

checkASD_HOST()
{
#printf "==> Connectivity with ASD endpoint: \n"
ASD_URL=$(getProperty "aff.asd.service.ws.endpoint")
##printf ${ASD_URL}
checkCurl "${ASD_URL}" "ASD"
##checkConn ${ASD_URL}

}


checkOGWRESTs()
{

OGW_URL=$(getProperty "usm.integration.endpointurl")

#printf "==> Connectivity OGW bandwidthdetails REST: \n"
OGW_URL1=$(getProperty "aff.networkinterface.service.ws.endpoint.bandwidthdetails.resourceURI")
checkCurl "${OGW_URL}${OGW_URL1}" "OGW"

#printf "==> Connectivity OGW login REST: \n"
OGW_URL2=$(getProperty "aff.networkinterface.service.ws.endpoint.login.resourceURI")
checkCurl "${OGW_URL}${OGW_URL2}" "OGW2"

#printf "==> Connectivity OGW tndetails REST: \n"
OGW_URL3=$(getProperty "aff.networkinterface.service.ws.endpoint.tndetails.resourceURI")
checkCurl "${OGW_URL}${OGW_URL3}" "OGW3"

#printf "==> Connectivity OGW getLocationDetails REST: \n"
OGW_URL4=$(getProperty "aff.OGW.service.ws.endpoint.getLocationDetails.resourceURI")
checkCurl "${OGW_URL}${OGW_URL4}" "OGW4"

#printf "==> Connectivity OGW getReservation REST: \n"
OGW_URL5=$(getProperty "aff.OGW.service.ws.endpoint.getReservation.resourceURI")
checkCurl "${OGW_URL}${OGW_URL5}" "OGW5"

#printf "==> Connectivity OGW bookAppointment REST: \n"
OGW_URL6=$(getProperty "aff.OGW.service.ws.endpoint.bookAppointment.resourceURI")
checkCurl "${OGW_URL}${OGW_URL6}" "OGW6"

#printf "==> Connectivity OGW cancelReservation REST: \n"
OGW_URL7=$(getProperty "aff.OGW.service.ws.endpoint.cancelReservation.resourceURI")
checkCurl "${OGW_URL}${OGW_URL7}" "OGW7"

#printf "==> Connectivity OGW cancelAppointment REST: \n"
OGW_URL8=$(getProperty "aff.OGW.service.ws.endpoint.cancelAppointment.resourceURI")
checkCurl "${OGW_URL}${OGW_URL8}" "OGW8"

#printf "==> Connectivity OGW InvokeWorkOrderRequest REST: \n"
OGW_URL9=$(getProperty "aff.OGW.service.ws.endpoint.InvokeWorkOrderRequest.resourceURI")
checkCurl "${OGW_URL}${OGW_URL9}" "OGW9"

}


checkCRMRESTs()
{

CRM_URL=$(getProperty "aff.OGW.service.ws.endpoint.baseURL")

#printf "==> Connectivity OGW getLocationDetails RESTs: \n"
CRM_URL1=$(getProperty "aff.OGW.service.ws.endpoint.getLocationDetails.resourceURI")
checkCurl "${CRM_URL}${CRM_URL1}" "CRM_URL"

#printf "==> Connectivity OGW login RESTs: \n"
CRM_URL2=$(getProperty "aff.networkinterface.service.ws.endpoint.login.resourceURI")
checkCurl "${CRM_URL}${CRM_URL2}" "CRM_URL2"

#printf "==> Connectivity OGW tndetails RESTs: \n"
CRM_URL3=$(getProperty "aff.networkinterface.service.ws.endpoint.tndetails.resourceURI")
checkCurl "${CRM_URL}${CRM_URL3}" "CRM_URL3"

}


checkOGWService()
{
#printf "==> Connectivity OGW Service endpoint: \n"
OGW_URL=$(getProperty "aff.orderGateway.service.ws.endpoint")
checkCurl ${OGW_URL} "OGW"
}

checkUSMConn()
{
#printf "==> Connectivity with USM endpoint: \n"
USM_URL=$(getProperty "usm.integration.endpointurl")
checkCurl ${USM_URL} "USM"
}

checkCPQREST()
{

CPQ_URL=$(getProperty "aff.cpq.service.ws.endpoint.baseURL")
#printf "==> Connectivity CPQ login RESTs: \n"
CPQ_URL1=$(getProperty "aff.cpq.service.ws.endpoint.login.resourceURI")
checkCurl "${CPQ_URL}${CPQ_URL1}" "CPQ_URL1"
#printf "==> Connectivity OGW completeAgreement RESTs: \n"
CPQ_URL2=$(getProperty "aff.cpq.service.ws.endpoint.completeAgreement.resourceURI")
checkCurl "${CPQ_URL}${CPQ_URL2}" "CPQ_URL2"

}


clear
printf "########################################\n"
printf "##  Running Environment Health Check  ##\n"
printf "########################################\n"
checkDiskSpace
checkCPQEndpoint
checkAmilUrlForCpm
checkAmilUrlForEmm
checkLIC_SQO_HOST
checkSQO_HOST
checkASD_HOST
checkOGWRESTs
checkOGWService
checkUSMConn
checkCPQREST
